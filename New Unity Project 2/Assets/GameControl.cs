﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameControl : MonoBehaviour {
	//public string stringToEdit = "Player Name";
	public static GameControl control;
	public int level;
	public GameState gs;
	public GameObject cc;
	public CamControl ccontrol;
	public Text playerName;
	public bool newPlayer;
	public bool delAll;
	public enum GameState

	{
		Menu,
		Pause,
		EndGame,
		Game
	}



	void Awake(){
		if(control == null){
			control = this;
			DontDestroyOnLoad (transform.gameObject);
		}
		else{
			Destroy (transform.gameObject);
		}


	}

void Start(){
		if (delAll) {
			DelPrefs ();
		}
		cc = GameObject.FindWithTag ("CamControl");
		ccontrol = cc.GetComponent<CamControl> ();
		if (!Check("Player Name")) {
			ShowPanels.control.ShowNamePanel ();
		} else {
			//ShowPanels.control.HideNamePanel ();
			playerName.text = GetPref("Player Name");
		}
			
	}

	public string GetPref(string pref){
		return  PlayerPrefs.GetString (pref);
	} 
	public void DelPrefs(){
		PlayerPrefs.DeleteAll ();
	}

	public void SetName(string pref,string pname){
//		print("we got name "+ pname);
		playerName.text=pname;
		ShowPanels.control.HideNamePanel ();
		PlayerPrefs.SetString (pref, pname);
	}
	public bool Check (string pref){
		return PlayerPrefs.HasKey (pref);
	}

public void LoadMain(){
		gs = GameState.Menu;
		level = 0;
		LoadLevel ();
		ShowPanels.control.HideGamePanel ();
		ShowPanels.control.ShowMenu ();
	}


public void LoadNextLevel(){
		level++;
		LoadLevel ();
		gs = GameState.Game;
	}

public void LoadLevel(){
		SceneManager.LoadScene (level);
	}
public void LoadLevel(int dlevel){
		level = dlevel;
		SceneManager.LoadScene (level);
		gs = GameState.Game;
	}
public void FixedUpdate(){
		if (gs == GameState.Game ) {
			if (!ccontrol.cam2.activeSelf) {
				ccontrol.ChangeCamera ();
			}
		}
			
	}

}