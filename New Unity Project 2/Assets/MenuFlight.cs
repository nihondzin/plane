﻿using UnityEngine;
using System.Collections;

public class MenuFlight : MonoBehaviour {
	public AudioSource audiofx;
	public float planeSpeed = 1000f;
	public float rotationSpeed = 200f;
	private bool crushed = false;

	void FixedUpdate()
	{
		if (!crushed)
		{
			Quaternion AddRotation = Quaternion.identity;
			float roll = 0;
			float pitch = 0;
			float yaw = 0;

			AddRotation.eulerAngles = new Vector3 (-pitch, yaw, -roll);
			GetComponent<Rigidbody> ().rotation *= AddRotation;
			Vector3 AddPosition = Vector3.forward;
			AddPosition = GetComponent<Rigidbody> ().rotation * AddPosition;
			GetComponent<Rigidbody> ().velocity = AddPosition * (Time.fixedDeltaTime * planeSpeed);



		}
	}

}

