﻿using UnityEngine;
using System.Collections;

public class CamControl : MonoBehaviour {
	
	public GameObject cam1, cam2;

	// Use this for initialization
	void Awake () {
		cam1 = GameObject.Find ("Main Camera");
		cam2 = GameObject.FindWithTag ("MultiCam");

		cam2.SetActive (false);

	}
	

	public void ChangeCamera(){
		if (cam1.activeSelf) {
			cam1.SetActive (false);
			cam2.SetActive (true);
		}
		else {
			cam2.SetActive (false);
			cam1.SetActive (true);
		}
	}
}
