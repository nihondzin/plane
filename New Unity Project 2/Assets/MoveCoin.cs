﻿using System.Collections;
using UnityEngine;

public class MoveCoin : MonoBehaviour {
	private Vector3 origin;
	private Rigidbody rb;

	// Use this for initialization
	void Awake () {
		rb = transform.GetComponent<Rigidbody> ();
		origin = rb.position; 
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3(Time.deltaTime*50.0f,0,0));
		Vector3 offset = new Vector3 (0,Mathf.Sin (Time.time)*6.0f, 0);
		rb.MovePosition (origin + offset);

	}
}
