﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;

public class FlightControl : MonoBehaviour {
	public GameObject explosion;
	public AudioSource audiofx;
	public float PlaneSpeed, speed;
	public Text countText;
	public float RotationSpeed;
	private bool crushed=false;
	static int count;



	void Start() {
		count = 0;
		SetCountText ();
		PlaneSpeed = speed = 600.0f;
		RotationSpeed = 200.0f;

	}

	void OnCollisionEnter(Collision collision)
	{
		ContactPoint contact = collision.contacts [0];

		Quaternion rot = Quaternion.FromToRotation (Vector3.up, contact.normal);
		Vector3 pos = contact.point;
		Instantiate (explosion, pos, rot);
	}


	void FixedUpdate()
	{
		if (!crushed)
		{
			Quaternion AddRotation = Quaternion.identity;
			float roll = 0;
			float pitch = 0;
			float yaw = 0;


			#if UNITY_EDITOR
			roll = Input.GetAxis ("Roll") * (Time.fixedDeltaTime * RotationSpeed);
			pitch = Input.GetAxis ("Pitch") * (Time.fixedDeltaTime * RotationSpeed);
			yaw = Input.GetAxis ("Yaw") * (Time.fixedDeltaTime * RotationSpeed);
			speed += Input.GetAxis("Jump")*(Time.fixedDeltaTime * PlaneSpeed);


			#else
			roll = CrossPlatformInputManager.GetAxis ("Horizontal") * (Time.fixedDeltaTime * RotationSpeed);
			pitch = CrossPlatformInputManager.GetAxis ("Vertical") * (Time.fixedDeltaTime * RotationSpeed);
			yaw = CrossPlatformInputManager.GetAxis ("Horizontal") * (Time.fixedDeltaTime * RotationSpeed);
			#endif

			AddRotation.eulerAngles = new Vector3 (-pitch, yaw, -roll);


			GetComponent<Rigidbody> ().rotation *= AddRotation;
			Vector3 AddPosition = Vector3.forward;
			AddPosition = GetComponent<Rigidbody> ().rotation * AddPosition;
			GetComponent<Rigidbody> ().velocity = AddPosition * (Time.fixedDeltaTime * speed);



		}
	}

	void SetCountText ()
	{
		countText.text = "Score: " + count.ToString ();
	}
	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ( "coin"))
		{
			other.gameObject.SetActive (false);
			count = count + 10;
			SetCountText ();
		}
	}
}
