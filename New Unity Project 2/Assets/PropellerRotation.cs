﻿using UnityEngine;
using System.Collections;

public class PropellerRotation : MonoBehaviour {
	public  GameObject speed;
	private FlightControl fc;


	void Awake (){
		fc = speed.GetComponent<FlightControl> ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up, fc.speed*Time.deltaTime*500);
	}
}
