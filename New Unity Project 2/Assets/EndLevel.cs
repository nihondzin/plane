﻿using UnityEngine;
using System.Collections;

public class EndLevel : MonoBehaviour {

	// Use this for initialization

	void OnTriggerEnter(Collider other){
		GameControl.control.LoadNextLevel ();
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
